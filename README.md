# Overview
This [Spigot](https://www.spigotmc.org/wiki/spigot/)-Plugin will allow placement of special-signs
which will show informations on the village it is located in.

# Description
Just place a (standing or hanging) sign somewhere in village-boundaries, with the following content/placeholder:

	##villageinfo##

Starting from this placeholder all lines will be replaced by the plugin with the following informations:

	V: ${villagers.count} / D: ${doors.count}
	M: ${villagers.maxcount} / G: ${golems.count}
	C: ${village.center.x}, ${village.center.y}, ${village.center.z}
	R: ${village.radius}

The village-info will update on each click on the villageinfo-sign - as long as the server will not be restarted.
If a server-restart has been performed, one has to rebuild the villageinfo-sign. 

# Installation
* Place the resulting jar-file in the plugins-folder of your Spigot-Installation.

# Commands
This mod does not have any commands. It just works. ;-)

# <a id="Todo"></a>Todo
* Save metadata for identifing villageinfo-signs in NBT-Tags (if possible)

# <a id="KnownBugs"></a>Known bugs
* Metadata will not be restored after server-restart.

# License
See [LICENSE](LICENSE) or the file META-INF/LICENSE shipped in the jar-file.