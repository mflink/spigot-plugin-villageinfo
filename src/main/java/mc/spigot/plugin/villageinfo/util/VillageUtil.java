package mc.spigot.plugin.villageinfo.util;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import net.minecraft.server.v1_12_R1.PersistentVillage;
import net.minecraft.server.v1_12_R1.Village;
import net.minecraft.server.v1_12_R1.World;

public class VillageUtil {
	private static final String NBT_VILLAGE_DOORS = "Doors";
	private static final String NBT_VILLAGE_CENTER_X = "CX";
	private static final String NBT_VILLAGE_CENTER_Y = "CY";
	private static final String NBT_VILLAGE_CENTER_Z = "CZ";
	private static final String NBT_VILLAGE_RADIUS = "Radius";
	private static final String NBT_VILLAGE_GOLEMSCOUNT = "Golems";
	private static final String NBT_VILLAGE_VILLAGERSCOUNT = "PopSize";
	private static final int VILLAGE_DETECT_RADIUS = 33; // Based on the idea that villages can grow up to 64x64 blocks, this is (64 / 2) + 1

	private static PersistentVillage getPersistentVillageAt(final World world) {
		// HACK: This is a hack and a very bad one. It might change in future releases of Minecraft-Server. Should be solved better if future releases of spigot/bukkit support Village-Operations! 
		final PersistentVillage persistentVillage = world.ak(); // ak := getPersistentVillage

		return persistentVillage;
	}

	private static NBTTagCompound getNBTData(final Village village) {
		final NBTTagCompound nbtTagCompound = new NBTTagCompound();

		// HACK: This is a hack and a very bad one. It might change in future releases of Minecraft-Server. Should be solved better if future releases of spigot/bukkit support Village-Operations! 
		village.b(nbtTagCompound); // b := getNbtData (or something like that)

		return nbtTagCompound;
	}

	private static PersistentVillage getPersistentVillageAt(final Location location) {
		final World world = (World) ((CraftWorld) location.getWorld()).getHandle();
		final PersistentVillage persistentVillage = getPersistentVillageAt(world);

		return persistentVillage;
	}

	public static Village getVillageAt(final Location location) {
		final PersistentVillage persistentVillage = getPersistentVillageAt(location);

		if (persistentVillage == null) {
			return null;
		}

		final Village village = persistentVillage.getClosestVillage(
				new BlockPosition(location.getX(), location.getY(), location.getZ()), VILLAGE_DETECT_RADIUS);

		return village;
	}

	public static List<Village> getVillages(final Location location) {
		final PersistentVillage persistentVillage = getPersistentVillageAt(location);

		if (persistentVillage == null) {
			return null;
		}

		return persistentVillage.getVillages();
	}

	public static int getVillagersCount(final Village village) {
		final NBTTagCompound nbtTagCompound = getNBTData(village);

		return nbtTagCompound.getInt(NBT_VILLAGE_VILLAGERSCOUNT);
	}

	public static int getGolemsCount(final Village village) {
		final NBTTagCompound nbtTagCompound = getNBTData(village);

		return nbtTagCompound.getInt(NBT_VILLAGE_GOLEMSCOUNT);
	}

	public static int getRadius(final Village village) {
		final NBTTagCompound nbtTagCompound = getNBTData(village);

		return nbtTagCompound.getInt(NBT_VILLAGE_RADIUS);
	}

	public static Vector getCenter(final Village village) {
		final NBTTagCompound nbtTagCompound = getNBTData(village);

		return new Vector(nbtTagCompound.getInt(NBT_VILLAGE_CENTER_X), nbtTagCompound.getInt(NBT_VILLAGE_CENTER_Y),
				nbtTagCompound.getInt(NBT_VILLAGE_CENTER_Z));
	}

	public static int getDoorsCount(final Village village) {
		final NBTTagCompound nbtTagCompound = getNBTData(village);
		final NBTTagList doors = (NBTTagList) nbtTagCompound.get(NBT_VILLAGE_DOORS);

		if (doors == null || doors.isEmpty()) {
			return 0;
		}

		return doors.size();
	}
}
