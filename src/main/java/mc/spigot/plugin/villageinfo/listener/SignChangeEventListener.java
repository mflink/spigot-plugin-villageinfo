package mc.spigot.plugin.villageinfo.listener;

import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.metadata.FixedMetadataValue;

import mc.spigot.plugin.villageinfo.SpigotPluginVillageInfo;

public class SignChangeEventListener implements Listener {
	private final SpigotPluginVillageInfo plugin;

	public SignChangeEventListener(final SpigotPluginVillageInfo plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onSignChangeEvent(final SignChangeEvent event) {
		for (int l = 0; l < event.getLines().length; ++l) {
			final String line = event.getLine(l);

			if ((line != null) && !line.isEmpty()
					&& line.trim().equalsIgnoreCase(SpigotPluginVillageInfo.PLACEHOLDER_VILLAGEINFO)) {
				// Ok. Found placeholder. Starting from this line, we fill all upcoming lines with informations.
				final Sign sign = (Sign) event.getBlock().getState();
				sign.setMetadata(SpigotPluginVillageInfo.PLACEHOLDER_VILLAGEINFO, new FixedMetadataValue(plugin, l));
				plugin.getLogger().info("Creating villageinfo at " + sign.getLocation().getBlockX() + ", " + sign.getLocation().getBlockY() + ", " + sign.getLocation().getZ());
				plugin.injectVillageInfo(sign, event.getLines(), l);

				break;
			}
		}
	}
}
