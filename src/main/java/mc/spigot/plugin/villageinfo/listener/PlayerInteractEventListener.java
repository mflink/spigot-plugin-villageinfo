package mc.spigot.plugin.villageinfo.listener;

import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import mc.spigot.plugin.villageinfo.SpigotPluginVillageInfo;

public class PlayerInteractEventListener implements Listener {
	private final SpigotPluginVillageInfo plugin;

	public PlayerInteractEventListener(final SpigotPluginVillageInfo plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerInteractEvent(final PlayerInteractEvent event) {

		if (event.hasBlock()) {
			final BlockState state = event.getClickedBlock().getState();

			if (state instanceof Sign) {
				final Sign sign = (Sign) state;
				plugin.getLogger().info("Player clicked a sign at " + sign.getLocation().getBlockX() + ", "
						+ sign.getLocation().getBlockY() + ", " + sign.getLocation().getZ());

				if (sign.hasMetadata(SpigotPluginVillageInfo.PLACEHOLDER_VILLAGEINFO)
						&& !sign.getMetadata(SpigotPluginVillageInfo.PLACEHOLDER_VILLAGEINFO).isEmpty()) {
					plugin.getLogger().info("Updating villageinfo at " + sign.getLocation().getBlockX() + ", "
							+ sign.getLocation().getBlockY() + ", " + sign.getLocation().getZ());
					plugin.injectVillageInfo(sign, sign.getLines(),
							sign.getMetadata(SpigotPluginVillageInfo.PLACEHOLDER_VILLAGEINFO).get(0).asInt());
				}
			}
		}
	}
}
