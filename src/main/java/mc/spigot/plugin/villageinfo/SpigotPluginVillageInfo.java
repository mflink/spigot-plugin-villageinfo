package mc.spigot.plugin.villageinfo;

import org.bukkit.block.Sign;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import mc.spigot.plugin.villageinfo.listener.PlayerInteractEventListener;
import mc.spigot.plugin.villageinfo.listener.SignChangeEventListener;
import mc.spigot.plugin.villageinfo.util.VillageUtil;
import net.minecraft.server.v1_12_R1.Village;

public class SpigotPluginVillageInfo extends JavaPlugin {
	public static final String PLACEHOLDER_VILLAGEINFO = "##villageinfo##";

	@Override
	public void onEnable() {
		super.onEnable();
		getLogger().info("Registering Listener for Player-Interact-Events ...");
		this.getServer().getPluginManager().registerEvents(new PlayerInteractEventListener(this), this);
		getLogger().info("Registering Listener for Sign-Changes ...");
		this.getServer().getPluginManager().registerEvents(new SignChangeEventListener(this), this);
	}

	synchronized public void injectVillageInfo(final Sign sign, final String[] lines, final int startingLineIndex) {
		final Village village = VillageUtil.getVillageAt(sign.getLocation());
		int index = startingLineIndex;

		if (village == null) {
			// Ah. No village around. Get out of here!
			lines[index] = "No village here!";
			return;
		}

		final int villagersCount = VillageUtil.getVillagersCount(village);
		final int doorsCount = VillageUtil.getDoorsCount(village);

		lines[index] = "V: " + villagersCount + " / D: " + doorsCount;

		final int upcomingLines = lines.length - (startingLineIndex + 1);
		int remainingLines = upcomingLines;

		if (remainingLines > 0) {
			final int maxVillagersCount = (int) ((double) doorsCount * 0.35); // Got 0.35 from PathfinderGoalMakeLove.f(), which is something like canBreed()
			lines[++index] = "M: " + maxVillagersCount + " / G: " + VillageUtil.getGolemsCount(village);
			--remainingLines;
		}

		if (remainingLines > 0) {
			final Vector center = VillageUtil.getCenter(village);
			lines[++index] = "C: " + center.getBlockX() + ", " + center.getBlockY() + ", " + center.getBlockZ();
			--remainingLines;
		}

		if (remainingLines > 0) {
			lines[++index] = "R: " + VillageUtil.getRadius(village);
			--remainingLines;
		}
	}
}